package com.daparcoc.laboratorio44_aplicacion3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

public class VibrateReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Vibrator mVibrator =
                (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        mVibrator.vibrate(500);
        Log.d("THIS IS IT", "THIS IS IT");
    }
}
